package fr.amanganiello.service;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;

public interface UpdateCompanyBalance {

    void updateCompanyBalance(Company company, Distribution distribution);
}
