package fr.amanganiello.service;

import fr.amanganiello.model.Endowment;

public interface LoadEndowment {

    Endowment loadEndowment();
}
