package fr.amanganiello.service;

import fr.amanganiello.model.Endowment;

public interface SaveEndowment {

    void saveEndowment(Endowment endowment);
}
