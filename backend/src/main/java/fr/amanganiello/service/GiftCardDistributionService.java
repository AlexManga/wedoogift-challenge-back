package fr.amanganiello.service;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.model.Wallet;

import java.time.LocalDate;

public interface GiftCardDistributionService {

    Distribution distributeGiftCard(Company company, User user, LocalDate startDate, long balance, Wallet wallet);
}
