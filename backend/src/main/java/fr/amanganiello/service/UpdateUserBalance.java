package fr.amanganiello.service;

import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;

public interface UpdateUserBalance {

    void updateUserBalance(User user, Distribution distribution);
}
