package fr.amanganiello.service.internal;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.service.UpdateCompanyBalance;
import org.springframework.stereotype.Service;

@Service
public class UpdateCompanyBalanceImpl implements UpdateCompanyBalance {

    @Override
    public void updateCompanyBalance(Company company, Distribution distribution) {
        if (distribution.isValid()) {
            company.setBalance(company.getBalance() - distribution.getAmount());
        }
    }
}
