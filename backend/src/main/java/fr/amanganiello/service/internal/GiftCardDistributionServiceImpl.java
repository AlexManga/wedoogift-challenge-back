package fr.amanganiello.service.internal;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.model.Wallet;
import fr.amanganiello.service.GiftCardDistributionService;
import fr.amanganiello.service.UpdateCompanyBalance;
import fr.amanganiello.service.UpdateUserBalance;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class GiftCardDistributionServiceImpl implements GiftCardDistributionService {

    private UpdateCompanyBalance updateCompanyBalance;
    private UpdateUserBalance updateUserBalance;

    public GiftCardDistributionServiceImpl(UpdateCompanyBalance updateCompanyBalance, UpdateUserBalance updateUserBalance) {
        this.updateCompanyBalance = updateCompanyBalance;
        this.updateUserBalance = updateUserBalance;
    }

    @Override
    public Distribution distributeGiftCard(Company company, User user, LocalDate startDate, long amount, Wallet wallet) {
        checkBalanceOfTheCompany(company, amount);
        Distribution distribution = new Distribution(amount, startDate, wallet.computeEndDateValidation(startDate), company, user, wallet);

        updateCompanyBalance.updateCompanyBalance(company, distribution);
        updateUserBalance.updateUserBalance(user, distribution);

        return distribution;
    }

    private void checkBalanceOfTheCompany(Company company, long amount) {
        if (amount > company.getBalance()) {
            throw new IllegalStateException("The balance of the company doesn't allow to distribute gift cards");
        }
    }
}
