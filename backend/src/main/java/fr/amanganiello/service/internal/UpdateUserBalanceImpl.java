package fr.amanganiello.service.internal;

import fr.amanganiello.model.Balance;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.service.UpdateUserBalance;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdateUserBalanceImpl implements UpdateUserBalance {

    @Override
    public void updateUserBalance(User user, Distribution distribution) {
        if (distribution.isValid()) {
            Optional<Balance> optionalBalance = user.getBalances().stream()
                    .filter(balance -> balance.getWalletId() == distribution.getWalletId())
                    .findFirst();
            if (optionalBalance.isPresent()) {
                Balance balance = optionalBalance.get();
                balance.setAmount(balance.getAmount() + distribution.getAmount());
            } else {
                user.getBalances().add(new Balance(distribution.getWalletId(), distribution.getAmount()));
            }
        }
    }
}
