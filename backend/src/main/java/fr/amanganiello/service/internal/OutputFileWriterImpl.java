package fr.amanganiello.service.internal;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.amanganiello.model.Endowment;
import fr.amanganiello.service.SaveEndowment;
import fr.amanganiello.utils.ObjectMapperProvider;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class OutputFileWriterImpl implements SaveEndowment {

    public static final String OUTPUT_FILENAME = "../Level2/data/output.json";

    @Override
    public void saveEndowment(Endowment endowment) {
        try {
            ObjectMapper objectMapper = ObjectMapperProvider.getObjectMapper();
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream(OUTPUT_FILENAME), endowment);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
