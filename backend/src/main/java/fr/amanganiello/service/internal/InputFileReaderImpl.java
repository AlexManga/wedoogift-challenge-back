package fr.amanganiello.service.internal;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.amanganiello.model.Endowment;
import fr.amanganiello.service.LoadEndowment;
import fr.amanganiello.utils.ObjectMapperProvider;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;

@Service
public class InputFileReaderImpl implements LoadEndowment {

    private static final String INPUT_FILENAME = "../Level2/data/input.json";

    @Override
    public Endowment loadEndowment() {
        Endowment endowment = null;
        try {
            ObjectMapper objectMapper = ObjectMapperProvider.getObjectMapper();
            endowment = objectMapper.readValue(new FileInputStream(INPUT_FILENAME), Endowment.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return endowment;
    }
}
