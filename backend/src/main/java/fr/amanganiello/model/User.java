package fr.amanganiello.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class User {

    private int id;
    private static int idCounter = 1;

    @JsonProperty("balance")
    private List<Balance> balances;

    public User(List<Balance> balances) {
        this.id = idCounter;
        this.balances = balances;
        idCounter++;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(balances, user.balances);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balances);
    }
}
