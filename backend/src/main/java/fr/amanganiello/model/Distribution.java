package fr.amanganiello.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDate;
import java.util.Objects;

@JsonPropertyOrder({"id", "wallet_id", "amount", "start_date", "end_date", "company_id", "user_id"})
public class Distribution {

    private int id;
    private static int idCounter = 1;

    private long amount;

    @JsonProperty("wallet_id")
    private int walletId;

    @JsonProperty("start_date")
    private LocalDate startDate;

    @JsonProperty("end_date")
    private LocalDate endDate;

    @JsonProperty("company_id")
    private int companyId;

    @JsonProperty("user_id")
    private int userId;

    public Distribution(long amount, LocalDate startDate, LocalDate endDate, Company company, User user, Wallet wallet) {
        this.id = idCounter;
        this.walletId = wallet.getId();
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.companyId = company.getId();
        this.userId = user.getId();
        idCounter++;
    }

    public Distribution() {

    }

    public int getId() {
        return id;
    }

    public long getAmount() {
        return amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getUserId() {
        return userId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public int getWalletId() {
        return walletId;
    }

    @JsonIgnore
    public boolean isValid() {
        LocalDate today = LocalDate.now();
        return isStartDateEqualsOrBefore(today) && isEndDateEqualsOrAfter(today);
    }

    private boolean isStartDateEqualsOrBefore(LocalDate today) {
        return startDate.isEqual(today) || startDate.isBefore(today);
    }

    private boolean isEndDateEqualsOrAfter(LocalDate today) {
        return endDate.isEqual(today) || endDate.isAfter(today);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distribution that = (Distribution) o;
        return id == that.id && amount == that.amount && walletId == that.walletId && companyId == that.companyId && userId == that.userId && Objects.equals(startDate, that.startDate) && Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, walletId, startDate, endDate, companyId, userId);
    }
}
