package fr.amanganiello.model;

import java.util.Objects;

public class Company {

    private int id;
    private static int idCounter = 1;
    private String name;
    private long balance;

    public Company(String name, long balance) {
        this.id = idCounter;
        this.name = name;
        this.balance = balance;
        idCounter++;
    }

    public Company() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return id == company.id && balance == company.balance && Objects.equals(name, company.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, balance);
    }
}
