package fr.amanganiello.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

public class Endowment {

    private List<Company> companies;
    private List<User> users;
    private List<Distribution> distributions;

    @JsonInclude(NON_NULL)
    private List<Wallet> wallets;

    public Endowment(List<Company> companies, List<User> users, List<Distribution> distributions) {
        this(companies, users, distributions, Collections.emptyList());
    }

    public Endowment(List<Company> companies, List<User> users, List<Distribution> distributions, List<Wallet> wallets) {
        this.companies = companies;
        this.users = users;
        this.distributions = distributions;
        this.wallets = wallets;
    }

    public Endowment() {
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void addDistribution(Distribution distribution) {
        this.distributions.stream()
                .filter(distribution1 -> distribution1.getId() == distribution.getId())
                .findFirst()
                .ifPresent(this.distributions::remove);

        this.distributions.add(distribution);
    }

    public void clearWallets() {
        this.wallets = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Endowment endowment = (Endowment) o;
        return Objects.equals(companies, endowment.companies) && Objects.equals(users, endowment.users) && Objects.equals(distributions, endowment.distributions) && Objects.equals(wallets, endowment.wallets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companies, users, distributions, wallets);
    }
}
