package fr.amanganiello.model;

import java.time.LocalDate;
import java.util.Objects;

import static java.time.Month.MARCH;

public class Wallet {

    public static final int VALIDITY_PERIOD_IN_DAYS = 364;
    private int id;
    private static int idCounter = 1;
    private String name;
    private GiftCardType type;

    public Wallet(String name, GiftCardType type) {
        id = idCounter;
        this.name = name;
        this.type = type;
        idCounter++;
    }

    public Wallet() {
    }

    public int getId() {
        return id;
    }

    public GiftCardType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public LocalDate computeEndDateValidation(LocalDate startDate) {
        switch (type) {
            case FOOD:
                return LocalDate.of(startDate.getYear() + 1, MARCH, 1).minusDays(1);
            case GIFT:
            default:
                return startDate.plusDays(VALIDITY_PERIOD_IN_DAYS);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return id == wallet.id && Objects.equals(name, wallet.name) && type == wallet.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }
}
