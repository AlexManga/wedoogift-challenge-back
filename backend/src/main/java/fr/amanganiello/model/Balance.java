package fr.amanganiello.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

@JsonPropertyOrder({"wallet_id","amount"})
public class Balance {

    @JsonProperty("wallet_id")
    private int walletId;
    private long amount;

    public Balance(int walletId, long amount) {
        this.walletId = walletId;
        this.amount = amount;
    }

    public Balance() {
    }

    public int getWalletId() {
        return walletId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Balance balance = (Balance) o;
        return walletId == balance.walletId && amount == balance.amount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(walletId, amount);
    }
}
