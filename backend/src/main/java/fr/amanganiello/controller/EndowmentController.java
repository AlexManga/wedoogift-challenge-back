package fr.amanganiello.controller;

import fr.amanganiello.model.*;
import fr.amanganiello.model.exception.InvalidRequestException;
import fr.amanganiello.service.GiftCardDistributionService;
import fr.amanganiello.service.LoadEndowment;
import fr.amanganiello.service.SaveEndowment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;

@RestController
@RequestMapping("/endowment")
public class EndowmentController {

    private LoadEndowment loadEndowment;
    private GiftCardDistributionService distributionService;
    private SaveEndowment saveEndowment;
    private Endowment endowment;

    public EndowmentController(LoadEndowment loadEndowment, GiftCardDistributionService distributionService, SaveEndowment saveEndowment) {
        this.loadEndowment = loadEndowment;
        this.distributionService = distributionService;
        this.saveEndowment = saveEndowment;
    }

    @PostMapping("/initialize")
    public ResponseEntity<Endowment> initEndowment() {
        this.endowment = loadEndowment.loadEndowment();
        return ResponseEntity.of(ofNullable(endowment));
    }

    @PostMapping("/save")
    public @ResponseBody
    Endowment saveEndowment() {
        saveEndowment.saveEndowment(endowment);
        return endowment;
    }

    @GetMapping
    public @ResponseBody
    Endowment getEndowment() {
        return endowment;
    }


    @PutMapping("/distribute")
    public ResponseEntity<Endowment> distributeGiftCard(@RequestParam int companyId,
                                                        @RequestParam int userId,
                                                        @RequestParam int walletId,
                                                        @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                        @RequestParam long amount) throws InvalidRequestException {

        if (endowment == null) {
            throw new InvalidRequestException("current endowment is null, please do /initialize then distribute gift cards");
        }

        Company company = endowment.getCompanies().stream().filter(companyV -> companyId == companyV.getId()).findFirst().orElse(null);
        User user = endowment.getUsers().stream().filter(userV -> userId == userV.getId()).findFirst().orElse(null);
        Wallet wallet = endowment.getWallets().stream().filter(walletV -> walletId == walletV.getId()).findFirst().orElse(null);

        if (Stream.of(company, user, wallet).anyMatch(Objects::isNull)) {
            throw new InvalidRequestException("companyId, userId or walletId does not exist");
        }

        Distribution distribution = distributionService.distributeGiftCard(company, user, startDate, amount, wallet);
        endowment.addDistribution(distribution);
        return ResponseEntity.of(Optional.of(endowment));
    }

    @ExceptionHandler(InvalidRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String handle(InvalidRequestException e) {
        return e.getMessage();
    }

}
