Feature: Génération des donations

  @Level1
  Scenario: Distribution de cartes cadeaux aux salariés
    Given la liste des entreprises et des salariés contenue dans le fichier d'entrée
    And l'entreprise "Wedoogift" donne un montant de 50 € au salarié 1 à partir du "2020-09-16"
    And l'entreprise "Wedoogift" donne un montant de 100 € au salarié 2 à partir du "2020-08-01"
    And l'entreprise "Wedoofood" donne un montant de 1000 € au salarié 3 à partir du "2020-10-01"
    When les donations sont enregistrées
    Then le résultat des donations est visible dans le fichier de sortie

  @Level2
  Scenario: Distribution de cartes cadeaux et repas aux salariés
    Given la liste des entreprises et des salariés contenue dans le fichier d'entrée
    And l'entreprise "Wedoogift" octroie un bon "cadeau" de 50 € au salarié 1 à partir du "2020-09-16"
    And l'entreprise "Wedoogift" octroie un bon "cadeau" de 100 € au salarié 2 à partir du "2020-09-01"
    And l'entreprise "Wedoofood" octroie un bon "cadeau" de 1000 € au salarié 3 à partir du "2020-10-01"
    And l'entreprise "Wedoogift" octroie un bon "repas" de 250 € au salarié 1 à partir du "2021-05-01"
    When les donations sont enregistrées
    Then le résultat des donations est visible dans le fichier de sortie
