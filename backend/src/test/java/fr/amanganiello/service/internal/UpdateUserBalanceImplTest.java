package fr.amanganiello.service.internal;

import fr.amanganiello.model.Balance;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.model.Wallet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.DistributionTestData.*;
import static fr.amanganiello.testdata.WalletTestData.createWalletGitCard;
import static org.assertj.core.api.Assertions.assertThat;

class UpdateUserBalanceImplTest {

    @Test
    void should_update_user_balance_when_distrib_is_valid() {
        // setup
        Wallet walletGitCard = createWalletGitCard();
        Balance balance = new Balance(walletGitCard.getId(),200);
        User userWithBalance = new User(Arrays.asList(balance));
        Distribution distribution = createDistributionWithCompanyAndUserAndWallet(createWedoogiftCompanyWithBalance(200), userWithBalance, walletGitCard);

        UpdateUserBalanceImpl updateUserBalance = new UpdateUserBalanceImpl();

        // test
        updateUserBalance.updateUserBalance(userWithBalance, distribution);

        // assert
        assertThat(userWithBalance.getBalances().get(0).getAmount()).isEqualTo(300);
    }

    @Test
    void should_not_update_user_balance_when_distrib_is_invalid() {
        // setup
        Wallet walletGitCard = createWalletGitCard();
        Balance balance = new Balance(walletGitCard.getId(),100);
        User userWithBalance = new User(Arrays.asList(balance));
        Distribution distribution = createInvalidDistributionWithCompanyAndUserAndWallet(createWedoogiftCompanyWithBalance(200), userWithBalance, walletGitCard);
        UpdateUserBalanceImpl updateUserBalance = new UpdateUserBalanceImpl();

        // test
        updateUserBalance.updateUserBalance(userWithBalance, distribution);

        // assert
        assertThat(userWithBalance.getBalances().get(0).getAmount()).isEqualTo(100);
    }

}
