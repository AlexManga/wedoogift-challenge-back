package fr.amanganiello.service.internal;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.model.Wallet;
import fr.amanganiello.service.UpdateCompanyBalance;
import fr.amanganiello.service.UpdateUserBalance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.Period;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.UserTestData.createUserWithBalance;
import static fr.amanganiello.testdata.WalletTestData.createWalletGitCard;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GiftCardDistributionServiceImplTest {

    @InjectMocks
    private GiftCardDistributionServiceImpl service;

    @Mock
    private UpdateCompanyBalance updateCompanyBalance;

    @Mock
    private UpdateUserBalance updateUserBalance;

    @BeforeEach
    void setUp() {
        service = new GiftCardDistributionServiceImpl(updateCompanyBalance, updateUserBalance);
    }

    @Test
    void should_throw_exception_when_balance_is_higher_than_balance_company() {
        // setup
        Company wedoogiftCompany = createWedoogiftCompanyWithBalance(99);
        User user = createUserWithBalance();
        Wallet wallet = createWalletGitCard();
        LocalDate today = LocalDate.now();

        // test & assert
        IllegalStateException illegalStateException = assertThrows(IllegalStateException.class, () -> {
            service.distributeGiftCard(wedoogiftCompany, user, today, 100, wallet);
        });

        assertThat(illegalStateException.getMessage()).isEqualTo("The balance of the company doesn't allow to distribute gift cards");
    }

    @Test
    void should_distribute_gift_card_to_user_and_update_user_and_company_balance() {
        // setup
        Company wedoogiftCompany = createWedoogiftCompanyWithBalance(100);
        User user = createUserWithBalance();
        LocalDate startDate = LocalDate.of(2020, 5, 1);

        // test
        Distribution distribution = service.distributeGiftCard(wedoogiftCompany, user, startDate, 50, createWalletGitCard());

        // assert
        assertThat(distribution).isNotNull();
        assertThat(distribution.getCompanyId()).isEqualTo(wedoogiftCompany.getId());
        assertThat(distribution.getUserId()).isEqualTo(user.getId());
        assertThat(distribution.getAmount()).isEqualTo(50);
        assertThat(Period.between(distribution.getStartDate(), distribution.getEndDate()).getMonths()).isEqualTo(11);
        assertThat(Period.between(distribution.getStartDate(), distribution.getEndDate()).getDays()).isEqualTo(29);
        verify(updateCompanyBalance).updateCompanyBalance(wedoogiftCompany, distribution);
        verify(updateUserBalance).updateUserBalance(user, distribution);
    }

}
