package fr.amanganiello.service.internal;


import fr.amanganiello.model.Endowment;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class InputFileReaderImplTest {


    @Test
    void should_return_file_content() {
        // setup
        InputFileReaderImpl inputFileReader = new InputFileReaderImpl();

        // test
        Endowment endowment = inputFileReader.loadEndowment();

        // assert
        assertThat(endowment).isNotNull();
        assertThat(endowment.getUsers()).hasSize(3);
        assertThat(endowment.getCompanies()).hasSize(2);

    }
}
