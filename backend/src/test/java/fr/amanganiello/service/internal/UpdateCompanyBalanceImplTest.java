package fr.amanganiello.service.internal;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import org.junit.jupiter.api.Test;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.DistributionTestData.createDistributionWithCompanyAndUser;
import static fr.amanganiello.testdata.DistributionTestData.createInvalidDistributionWithCompanyAndUser;
import static fr.amanganiello.testdata.UserTestData.createUserWithBalance;
import static org.assertj.core.api.Assertions.assertThat;

class UpdateCompanyBalanceImplTest {

    @Test
    void should_update_company_balance_when_distrib_is_valid() {
        // setup
        User userWithBalance = createUserWithBalance();
        Company wedoogiftCompanyWithBalance = createWedoogiftCompanyWithBalance(200);
        Distribution distribution = createDistributionWithCompanyAndUser(wedoogiftCompanyWithBalance, userWithBalance);
        UpdateCompanyBalanceImpl updateCompanyBalance = new UpdateCompanyBalanceImpl();

        // test
        updateCompanyBalance.updateCompanyBalance(wedoogiftCompanyWithBalance, distribution);

        // assert
        assertThat(wedoogiftCompanyWithBalance.getBalance()).isEqualTo(100);
    }

    @Test
    void should_not_update_company_balance_when_distrib_is_invalid() {
        // setup
        User userWithBalance = createUserWithBalance();
        Company wedoogiftCompanyWithBalance = createWedoogiftCompanyWithBalance(200);
        Distribution distribution = createInvalidDistributionWithCompanyAndUser(wedoogiftCompanyWithBalance, userWithBalance);
        UpdateCompanyBalanceImpl updateCompanyBalance = new UpdateCompanyBalanceImpl();

        // test
        updateCompanyBalance.updateCompanyBalance(wedoogiftCompanyWithBalance, distribution);

        // assert
        assertThat(wedoogiftCompanyWithBalance.getBalance()).isEqualTo(200);
    }
}
