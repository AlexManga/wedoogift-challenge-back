package fr.amanganiello.service.internal;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static fr.amanganiello.service.internal.OutputFileWriterImpl.OUTPUT_FILENAME;
import static fr.amanganiello.testdata.EndowmentTestData.createEndowment;

class OutputFileWriterImplTest {

    @Test
    void should_save_endowment() {
        // setup
        OutputFileWriterImpl outputFileWriter = new OutputFileWriterImpl();

        // test
        outputFileWriter.saveEndowment(createEndowment());

        // assert
        Assertions.assertThat(getFileContent()).isNotBlank();
    }

    @AfterEach
    void tearDown() {
        new File(OUTPUT_FILENAME).delete();
    }

    private String getFileContent() {
        try (FileReader reader = new FileReader(OUTPUT_FILENAME);) {
            return reader.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
