package fr.amanganiello.controller;

import fr.amanganiello.model.Endowment;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EndowmentControllerIntegrationTest {


    private static final String USER = "user";
    private static final String PASSWORD = "password";

    @LocalServerPort
    private int port;
    private final static String BASE_URL = "http://localhost:";
    private final static String ENDOWMENT_ENDPOINT = "/endowment";
    private static Endowment endowment;
    private final RequestSpecification basicAuth = given().auth().basic(USER, PASSWORD);


    @Test
    @Order(1)
    void should_return_endowment_when_call_init() {
        // test
        endowment = basicAuth.when().post(BASE_URL + port + ENDOWMENT_ENDPOINT + "/initialize")
                .then()
                .statusCode(200)
                .extract().as(Endowment.class);

        assertThat(endowment).isNotNull();
    }

    @Test
    @Order(2)
    void distribute_gift_card() {
        // setup
        assertThat(endowment.getDistributions()).isEmpty();
        RequestSpecification requestSpecification = given()
                .queryParam("companyId", 1)
                .queryParam("userId", 1)
                .queryParam("walletId", 1)
                .queryParam("date", "2021-01-01")
                .queryParam("amount", 100)
                .auth().basic(USER, PASSWORD);

        // test
        endowment = requestSpecification
                .when()
                .put(BASE_URL + port + ENDOWMENT_ENDPOINT + "/distribute")
                .then()
                .statusCode(200)
                .extract().as(Endowment.class);

        //assert
        assertThat(endowment.getDistributions()).hasSize(1);
    }

    @Test
    @Order(3)
    void get_endowment() {
        // test
        endowment =
                basicAuth.when()
                        .get(BASE_URL + port + ENDOWMENT_ENDPOINT)
                        .then()
                        .statusCode(200)
                        .extract().as(Endowment.class);

        //assert
        assertThat(endowment).isNotNull();
    }

    @Test
    @Order(4)
    void save_endowment() {
        // test
        endowment =
                basicAuth.when()
                        .post(BASE_URL + port + ENDOWMENT_ENDPOINT + "/save")
                        .then()
                        .statusCode(200)
                        .extract().as(Endowment.class);

        //assert
        assertThat(endowment).isNotNull();
    }


    @ParameterizedTest
    @CsvSource({"99,1,1", "1,99,1", "1,1,99"})
    @Order(5)
    void should_return_error_when_id_is_invalid(int companyId, int userId, int walletId) {
        // setup
        RequestSpecification requestSpecification = given()
                .queryParam("companyId", companyId)
                .queryParam("userId", userId)
                .queryParam("walletId", walletId)
                .queryParam("date", "2021-01-01")
                .queryParam("amount", 100)
                .auth().basic(USER, PASSWORD);

        // test
        String errorMessage = requestSpecification
                .when()
                .put(BASE_URL + port + ENDOWMENT_ENDPOINT + "/distribute")
                .then()
                .statusCode(400)
                .extract().asString();

        //assert
        assertThat(errorMessage)
                .isNotNull()
                .isEqualTo("companyId, userId or walletId does not exist");
    }

    @Test
    @Order(-1)
    void should_return_error_when_endowment_is_null() {
        // setup
        RequestSpecification requestSpecification = given()
                .queryParam("companyId", 1)
                .queryParam("userId", 1)
                .queryParam("walletId", 1)
                .queryParam("date", "2021-01-01")
                .queryParam("amount", 100)
                .auth().basic(USER, PASSWORD);

        // test
        String errorMessage = requestSpecification
                .when()
                .put(BASE_URL + port + ENDOWMENT_ENDPOINT + "/distribute")
                .then()
                .statusCode(400)
                .extract().asString();

        //assert
        assertThat(errorMessage)
                .isNotNull()
                .isEqualTo("current endowment is null, please do /initialize then distribute gift cards");
    }

    @Test
    @Order(0)
    void should_return_unauthorized_when_credentials_are_not_provided() {
        // test & assert
        when()
                .get(BASE_URL + port + ENDOWMENT_ENDPOINT)
                .then()
                .statusCode(401);
    }


}
