package fr.amanganiello.cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.amanganiello.model.*;
import fr.amanganiello.service.GiftCardDistributionService;
import fr.amanganiello.service.SaveEndowment;
import fr.amanganiello.service.UpdateCompanyBalance;
import fr.amanganiello.service.UpdateUserBalance;
import fr.amanganiello.service.internal.*;
import fr.amanganiello.utils.ObjectMapperProvider;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;

import static fr.amanganiello.service.internal.OutputFileWriterImpl.OUTPUT_FILENAME;
import static org.assertj.core.api.Assertions.assertThat;

public class ChallengesSteps {

    private final InputFileReaderImpl loadEndowment;
    private final SaveEndowment saveEndowment;
    private final GiftCardDistributionService endowmentService;
    private final UpdateCompanyBalance updateCompanyBalance;
    private final UpdateUserBalance updateUserBalance;
    private Endowment endowment;

    public ChallengesSteps() {
        loadEndowment = new InputFileReaderImpl();
        saveEndowment = new OutputFileWriterImpl();
        updateCompanyBalance = new UpdateCompanyBalanceImpl();
        updateUserBalance = new UpdateUserBalanceImpl();
        endowmentService = new GiftCardDistributionServiceImpl(updateCompanyBalance, updateUserBalance);
    }

    @Given("la liste des entreprises et des salariés contenue dans le fichier d'entrée")
    public void load_endowment() {
        endowment = loadEndowment.loadEndowment();
    }

    @And("l'entreprise {string} donne un montant de {int} € au salarié {int} à partir du {string}")
    public void distribute_gift_cards(String companyName, int amount, int userId, String startDate) {
        Distribution distribution = endowmentService.distributeGiftCard(getCompany(companyName), getUser(userId), LocalDate.parse(startDate), amount, null);
        endowment.addDistribution(distribution);
    }

    private User getUser(int userId) {
        return endowment.getUsers().stream().filter(user -> userId == user.getId()).findFirst().get();
    }

    private Company getCompany(String companyName) {
        return endowment.getCompanies().stream().filter(company -> companyName.equals(company.getName())).findFirst().get();
    }

    @When("les donations sont enregistrées")
    public void save_endowment() {
        endowment.clearWallets();
        saveEndowment.saveEndowment(endowment);
    }

    @Then("le résultat des donations est visible dans le fichier de sortie")
    public void results_should_be_visible_in_file() throws IOException {
        ObjectMapper objectMapper = ObjectMapperProvider.getObjectMapper();
        Endowment endowmentResult = objectMapper.readValue(new FileInputStream(OUTPUT_FILENAME), Endowment.class);
        endowment.clearWallets();
        assertThat(endowment).isEqualTo(endowmentResult);
    }

    @And("l'entreprise {string} octroie un bon {string} de {int} € au salarié {int} à partir du {string}")
    public void lEntrepriseOctroieUnBonCadeauDe€AuSalariéÀPartirDu(String companyName, String cardType, int giftAmount, int userId, String startDate) {
        int walletId = getWalletId(cardType);
        Distribution distribution = endowmentService.distributeGiftCard(getCompany(companyName), getUser(userId), LocalDate.parse(startDate), giftAmount, getWalletById(walletId));
        endowment.addDistribution(distribution);
    }

    private Wallet getWalletById(int walletId) {
        return endowment.getWallets().stream().filter(wallet -> walletId == wallet.getId()).findFirst().orElse(null);
    }

    private int getWalletId(String cardType) {
        switch (cardType) {
            case "repas":
                return 2;
            case "cadeau":
            default:
                return 1;
        }
    }
}
