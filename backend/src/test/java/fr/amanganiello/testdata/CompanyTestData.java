package fr.amanganiello.testdata;

import fr.amanganiello.model.Company;

public class CompanyTestData {

    public static Company createWedoogiftCompanyWithBalance(long balance) {
        return new Company("Wedoogift", balance);
    }
}
