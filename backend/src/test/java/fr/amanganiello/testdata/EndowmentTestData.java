package fr.amanganiello.testdata;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.Endowment;
import fr.amanganiello.model.User;

import java.util.ArrayList;
import java.util.List;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.DistributionTestData.createDistributionWithCompanyAndUser;
import static fr.amanganiello.testdata.UserTestData.createUserWithBalance;

public class EndowmentTestData {

    public static Endowment createEndowment() {
        List<Company> companies = new ArrayList<>();
        Company company = createWedoogiftCompanyWithBalance(100);
        companies.add(company);

        List<User> users = new ArrayList<>();
        User user = createUserWithBalance();
        users.add(user);

        List<Distribution> distributions = new ArrayList<>();
        distributions.add(createDistributionWithCompanyAndUser(company, user));

        return new Endowment(companies, users, distributions);
    }
}
