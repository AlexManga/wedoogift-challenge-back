package fr.amanganiello.testdata;

import fr.amanganiello.model.Company;
import fr.amanganiello.model.Distribution;
import fr.amanganiello.model.User;
import fr.amanganiello.model.Wallet;

import java.time.LocalDate;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.UserTestData.createUserWithBalance;
import static fr.amanganiello.testdata.WalletTestData.createWalletGitCard;

public class DistributionTestData {

    public static Distribution createDistribution() {
        return createDistributionWithCompanyAndUser(createWedoogiftCompanyWithBalance(1000), createUserWithBalance());
    }

    public static Distribution createInvalidDistribution() {
        return createInvalidDistributionWithCompanyAndUser(createWedoogiftCompanyWithBalance(1000), createUserWithBalance());
    }

    public static Distribution createInvalidDistributionWithCompanyAndUser(Company company, User user) {
        LocalDate oldDate = LocalDate.of(2019,1,1);
        return new Distribution(100, oldDate, oldDate.plusYears(1), company, user, createWalletGitCard());
    }

    public static Distribution createInvalidDistributionWithCompanyAndUserAndWallet(Company company, User user, Wallet wallet) {
        LocalDate oldDate = LocalDate.of(2019,1,1);
        return new Distribution(100, oldDate, oldDate.plusYears(1), company, user, wallet);
    }

    public static Distribution createDistributionWithCompanyAndUser(Company company, User user) {
        LocalDate now = LocalDate.now().minusDays(1);
        return new Distribution(100, now, now.plusYears(1), company, user, createWalletGitCard());
    }

    public static Distribution createDistributionWithCompanyAndUserAndWallet(Company company, User user, Wallet wallet) {
        LocalDate now = LocalDate.now().minusDays(1);
        return new Distribution(100, now, now.plusYears(1), company, user, wallet);
    }
}
