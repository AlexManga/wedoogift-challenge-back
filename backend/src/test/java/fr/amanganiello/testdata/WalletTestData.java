package fr.amanganiello.testdata;

import fr.amanganiello.model.GiftCardType;
import fr.amanganiello.model.Wallet;

public class WalletTestData {

    public static Wallet createWalletGitCard() {
        return new Wallet("gift card", GiftCardType.GIFT);
    }

    public static Wallet createWalletFoodCard() {
        return new Wallet("food card", GiftCardType.FOOD);
    }
}
