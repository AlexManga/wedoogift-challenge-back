package fr.amanganiello.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

class WalletTest {

    @Test
    void computeEndDateValidation_for_food_card() {
        // setup
        Wallet wallet = new Wallet("gift", GiftCardType.FOOD);

        // test
        LocalDate endDate = wallet.computeEndDateValidation(LocalDate.of(2021, 7, 31));

        // assert
        Assertions.assertThat(endDate.getYear()).isEqualTo(2022);
        Assertions.assertThat(endDate.getMonth()).isEqualTo(Month.FEBRUARY);
        Assertions.assertThat(endDate.getDayOfMonth()).isEqualTo(28);
    }

    @Test
    void computeEndDateValidation_for_gift_card() {
        // setup
        Wallet wallet = new Wallet("gift", GiftCardType.GIFT);

        // test
        LocalDate endDate = wallet.computeEndDateValidation(LocalDate.of(2021, 7, 31));

        // assert
        Assertions.assertThat(endDate.getYear()).isEqualTo(2022);
        Assertions.assertThat(endDate.getMonth()).isEqualTo(Month.JULY);
        Assertions.assertThat(endDate.getDayOfMonth()).isEqualTo(30);
    }
}
