package fr.amanganiello.model;

import fr.amanganiello.testdata.DistributionTestData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static fr.amanganiello.testdata.CompanyTestData.createWedoogiftCompanyWithBalance;
import static fr.amanganiello.testdata.UserTestData.createUserWithBalance;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

class EndowmentTest {

    @Test
    void should_add_distributions_when_is_not_present() {
        // setup
        Distribution distribution = DistributionTestData.createDistribution();

        List<Company> companies = singletonList(createWedoogiftCompanyWithBalance(100));
        List<User> users = singletonList(createUserWithBalance());

        Endowment endowment = new Endowment(companies, users, new ArrayList<>());

        // test
        endowment.addDistribution(distribution);

        //assert
        assertThat(endowment.getDistributions()).hasSize(1);
    }

    @Test
    void should_replace_distributions_when_is_present() {
        // setup
        Distribution distribution = DistributionTestData.createDistribution();

        List<Company> companies = singletonList(createWedoogiftCompanyWithBalance(100));
        List<User> users = singletonList(createUserWithBalance());
        List<Distribution> distributions = new ArrayList<>();
        distributions.add(distribution);

        Endowment endowment = new Endowment(companies, users, distributions);


        // test
        endowment.addDistribution(distribution);

        //assert
        assertThat(endowment.getDistributions()).hasSize(1);
        assertThat(endowment.getDistributions().get(0)).isEqualTo(distribution);
    }

    @Test
    void should_remove_wallets_when_call_clear() {
        // setup
        Distribution distribution = DistributionTestData.createDistribution();

        List<Company> companies = singletonList(createWedoogiftCompanyWithBalance(100));
        List<User> users = singletonList(createUserWithBalance());
        List<Distribution> distributions = new ArrayList<>();
        distributions.add(distribution);

        Endowment endowment = new Endowment(companies, users, distributions);


        // test
        endowment.clearWallets();

        //assert
        assertThat(endowment.getWallets()).isNull();
    }
}
